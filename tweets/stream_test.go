package tweets

import (
	"bytes"
	"strings"
	"testing"

	"github.com/dghubble/go-twitter/twitter"
	"github.com/fatih/color"
)

func TestHandleTweet(t *testing.T) {
	var blueBold = color.New(color.FgBlue, color.Bold).SprintFunc()
	var green = color.New(color.FgGreen).SprintFunc()
	var underlined = color.New(color.Underline).SprintFunc()
	var yellow = color.New(color.FgYellow).SprintFunc()

	tweet := twitter.Tweet{
		Text: "Stream tweets to your CLI https://gitlab.com/hackebrot/tweets #golang #twitter @francesc :)",
		User: &twitter.User{
			ScreenName: "hackebrot",
		},
		Entities: &twitter.Entities{
			Hashtags: []twitter.HashtagEntity{
				twitter.HashtagEntity{
					Text:    "golang",
					Indices: twitter.Indices{62, 69},
				},
				twitter.HashtagEntity{
					Text:    "twitter",
					Indices: twitter.Indices{70, 78},
				},
			},
			Urls: []twitter.URLEntity{
				twitter.URLEntity{
					URL:     "https://gitlab.com/hackebrot/tweets",
					Indices: twitter.Indices{26, 61},
				},
			},
			UserMentions: []twitter.MentionEntity{
				twitter.MentionEntity{
					ScreenName: "francesc",
					Indices:    twitter.Indices{79, 88},
				},
			},
		},
	}

	expected := strings.Join([]string{
		blueBold("@hackebrot"), ": Stream tweets to your CLI ",
		underlined("https://gitlab.com/hackebrot/tweets"), " ",
		yellow("#golang"), " ", yellow("#twitter"), " ",
		green("@francesc"), " :)\n\n",
	}, "")

	buf := &bytes.Buffer{}
	s := Stream{out: buf}
	s.handleTweet(&tweet)

	// read from the output
	got := buf.String()
	if got != expected {
		// Change format to %q rather than %v to see raw strings
		t.Errorf("handleTweet(t):\ngot:      %v\nexpected: %v", buf, expected)
	}
}
