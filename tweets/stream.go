package tweets

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"sort"

	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
	"github.com/fatih/color"
)

// modifier for highlighting tweet entities
type modifier struct {
	start, end int
	sprint     func(...interface{}) string
	text       string
}

// newModifier creates a modifier for a text of a tweet
func newModifier(i twitter.Indices, text string, attrs ...color.Attribute) *modifier {
	return &modifier{
		start:  i.Start(),
		end:    i.End(),
		sprint: color.New(attrs...).SprintFunc(),
		text:   text,
	}
}

// byStart sorts modifiers by start index
type byStart []*modifier

func (s byStart) Len() int           { return len(s) }
func (s byStart) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
func (s byStart) Less(i, j int) bool { return s[i].start < s[j].start }

// Stream is a stream of tweets.
type Stream struct {
	stream *twitter.Stream
	out    io.Writer
}

// Close closes the stream.
func (s *Stream) Close() { s.stream.Stop() }

// handleTweet writes a given tweet to Stdout
func (s *Stream) handleTweet(tweet *twitter.Tweet) {
	// ignore retweets and quoted tweets
	if tweet.RetweetedStatus != nil || tweet.QuotedStatus != nil {
		return
	}

	mods := []*modifier{}
	for _, u := range tweet.Entities.Urls {
		mods = append(mods, newModifier(u.Indices, u.URL, color.Underline))
	}
	for _, h := range tweet.Entities.Hashtags {
		mods = append(mods, newModifier(h.Indices, "#"+h.Text, color.FgYellow))
	}
	for _, m := range tweet.Entities.UserMentions {
		mods = append(mods, newModifier(m.Indices, "@"+m.ScreenName, color.FgGreen))
	}
	sort.Sort(byStart(mods))

	text := tweet.Text
	if len(mods) > 0 {
		// convert to runes to get the indices right
		textRunes := []rune(text)

		buf := &bytes.Buffer{}
		idx := 0
		for _, m := range mods {
			fmt.Fprintf(buf, "%s", string(textRunes[idx:m.start]))
			fmt.Fprintf(buf, "%s", m.sprint(m.text))
			idx = m.end
		}
		fmt.Fprintf(buf, "%s", string(textRunes[idx:]))
		text = buf.String()
	}

	blue := color.New(color.FgBlue, color.Bold).SprintFunc()
	fmt.Fprintf(s.out, "%v: %v\n\n", blue("@"+tweet.User.ScreenName), text)
}

// client creates a client based on the environment.
func client() (*twitter.Client, error) {
	keys := []string{
		"TWEETS_CONSUMER_KEY",
		"TWEETS_CONSUMER_SECRET",
		"TWEETS_ACCESS_TOKEN",
		"TWEETS_ACCESS_SECRET",
	}
	t := map[string]string{}
	for _, key := range keys {
		v := os.Getenv(key)
		if v == "" {
			return nil, fmt.Errorf("environment variable %q is required", key)
		}
		t[key] = v
	}

	config := oauth1.NewConfig(t["TWEETS_CONSUMER_KEY"], t["TWEETS_CONSUMER_SECRET"])
	token := oauth1.NewToken(t["TWEETS_ACCESS_TOKEN"], t["TWEETS_ACCESS_SECRET"])
	return twitter.NewClient(config.Client(oauth1.NoContext, token)), nil
}

// NewStream connects to the StreamingAPI and
// writes status updates to the given writer.
func NewStream(out io.Writer, track *string) (*Stream, error) {
	client, err := client()
	if err != nil {
		return nil, fmt.Errorf("could not create twitter client: %v", err)
	}

	filterParams := &twitter.StreamFilterParams{
		Track:         []string{*track},
		StallWarnings: twitter.Bool(true),
	}

	stream, err := client.Streams.Filter(filterParams)
	if err != nil {
		return nil, fmt.Errorf("could not start streaming: %v", err)
	}

	s := Stream{stream, out}
	demux := twitter.NewSwitchDemux()
	demux.Tweet = s.handleTweet

	go demux.HandleChan(stream.Messages)
	return &s, nil
}
