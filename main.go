package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/fatih/color"

	"gitlab.com/hackebrot/tweets/tweets"
)

func main() {
	track := flag.String("track", "#FOSS,golang blog", "a comma-separated list of phrases to track")
	flag.Parse()

	s, err := tweets.NewStream(os.Stdout, track)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(1)
	}
	defer s.Close()

	green := color.New(color.FgHiGreen, color.Bold).SprintfFunc()
	fmt.Fprint(os.Stdout, green("starting stream for keyword %q\n\n", *track))

	// Wait for CTRL-C
	ch := make(chan os.Signal)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
	<-ch

	red := color.New(color.FgHiRed, color.Bold).SprintfFunc()
	fmt.Fprint(os.Stdout, red("stopping stream for keyword %q\n\n", *track))
}
